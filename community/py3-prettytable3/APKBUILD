# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-prettytable3
pkgver=3.13.0
pkgrel=0
pkgdesc="Display tabular data in a visually appealing ASCII table format"
url="https://github.com/jazzband/prettytable"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-wcwidth !py3-prettytable"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs py3-installer"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-lazy-fixtures"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jazzband/prettytable/archive/$pkgver.tar.gz"
builddir="$srcdir/prettytable-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
83043d34a664806284916c1f7858b0e00695a48e2c61a574f2b1eaa2e539563119b10d80c4262b613b511fda5c2d5ecfb9bfa2b8f1082f88c79a6f3bd626a2fe  py3-prettytable3-3.13.0.tar.gz
"
